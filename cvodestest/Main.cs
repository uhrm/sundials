using System;

using nvector;
using sunmatrix;
using linearsolver;
using cvodes;

namespace cvodestest
{
    class Program
    {
        // Test problems are from
        //
        // L. G. Birta, M. Yang, and O. Abou-Rabia
        // An adaptive approach to stepsize control in ODE solvers
        // Mathematics and Computers in Simulation
        // Volume 35 Issue 1, Feb. 1993
        // Pages 63-78 

        private static int Birta1993P1(double t, NVector y, NVector ydot, IntPtr user_data)
        {
            ydot[0] = 1.0/Math.Sqrt(1.0 - t);
            return 0;
        }
        
        private static int Birta1993P2(double t, NVector y, NVector ydot, IntPtr user_data)
        {
            ydot[0] = y[1];
            ydot[1] = -2.0*y[0]*y[0]*(1.0-4.0*t*t*y[0]);
            return 0;
        }
        
        private static int Birta1993P3(double t, NVector y, NVector ydot, IntPtr user_data)
        {
            ydot[0] = y[0]/(2.0*(1.0+t)) - 2.0*t*y[1];
            ydot[1] = y[1]/(2.0*(1.0+t)) + 2.0*t*y[0];
            return 0;
        }
        
        private static int Birta1993P4(double t, NVector y, NVector ydot, IntPtr user_data)
        {
            double y02 = y[0]*y[0];
            double y12 = y[1]*y[1];
            double den = Math.Pow(y02+y12, 1.5);
            ydot[0] = y[2];
            ydot[1] = y[3];
            ydot[2] = -y[0]/den;
            ydot[3] = -y[1]/den;
            return 0;
        }
        
        private static double nr(double eps, double t, double x)
        {
            double xn = x;
            do
            {
                x = xn;
                xn = x - (x-eps*Math.Sin(x)-t)/(1.0-eps*Math.Cos(x));
            } while (Math.Abs(xn-x)/(1.0+Math.Abs(xn)) > 1e-14);
            return xn;
        }
        
        public static void Main(string[] args)
        {

            //
            // Birta 1993 Problem 1
            //
            
            Console.WriteLine();
            Console.WriteLine("Birta 1993 Problem 1");
            using (var y = new NVectorSerial(new double[] {-2.0}))
            using (var A = new SUNDenseMatrix(1, 1))
            using (var solver = new SUNLinSolDense(y, A))
            using (var cvode = new Solver())
            {
                var t = 0.0;
                cvode.Init(new CVRhsFn(Birta1993P1), t, y);
                cvode.SetTolerances(1e-6, 1e-6);
                cvode.SetLinearSolver(solver, A);
                Console.WriteLine($"{t:0.000}: {y[0]:0.000000}   {y[0]:0.000000}");
                for (int i=1; i<=199; i++)
                {
                    t = (double)i/200.0;
                    ReturnFlag flag = cvode.Step(t, y, out t, Task.Normal);
                    if (flag != ReturnFlag.Success)
                    {
                        Console.WriteLine("Error in \"Step\": {0}.", flag);
                        return;
                    }
                    Console.WriteLine($"{t:0.000}: {y[0]:0.000000}   {-2.0*Math.Sqrt(1.0-t):0.000000}");
                }
            }

            //
            // Birta 1993 Problem 2
            //

            Console.WriteLine();
            Console.WriteLine("Birta 1993 Problem 2");
            using (var y = new NVectorSerial(new double[] {1.0, 0.0}))
            using (var A = new SUNDenseMatrix(2, 2))
            using (var solver = new SUNLinSolDense(y, A))
            using (var cvode = new Solver())
            {
                var t = 0.0;
                cvode.Init(new CVRhsFn(Birta1993P2), t, y);
                cvode.SetTolerances(1e-10, 1e-10);
                cvode.SetLinearSolver(solver, A);
                Console.WriteLine($"{t,6:0.000}: {y[0],10:0.000000}   {y[0],10:0.000000}");
                Console.WriteLine($"        {y[1],10:0.000000}   {y[1],10:0.000000}");
                for (int i=1; i<=200; i++)
                {
                    t = (double)i*15.0/200.0;
                    ReturnFlag flag = cvode.Step(t, y, out t, Task.Normal);
                    if (flag != ReturnFlag.Success)
                    {
                        Console.WriteLine("Error in \"Step\": {0}.", flag);
                        return;
                    }
                    Console.WriteLine($"{t,6:0.000}: {y[0],10:0.000000}   {1.0/(1.0+t*t),10:0.000000}");
                    Console.WriteLine($"        {y[1],10:0.000000}   {-2.0*t/(1.0+t*t)/(1.0+t*t),10:0.000000}");
                }
            }

            //
            // Birta 1993 Problem 3
            //

            Console.WriteLine();
            Console.WriteLine("Birta 1993 Problem 3");
            using (var y = new NVectorSerial(new double[] {1.0, 0.0}))
            using (var A = new SUNDenseMatrix(2, 2))
            using (var solver = new SUNLinSolDense(y, A))
            using (var cvode = new Solver())
            {
                var t = 0.0;
                cvode.Init(new CVRhsFn(Birta1993P3), t, y);
                cvode.SetTolerances(1e-6, 1e-6);
                cvode.SetLinearSolver(solver, A);
                Console.WriteLine($"{t,6:0.000}: {y[0],10:0.000000}   {y[0],10:0.000000}");
                Console.WriteLine($"        {y[1],10:0.000000}   {y[1],10:0.000000}");
                for (int i=1; i<=200; i++)
                {
                    t = (double)i*6.0/200.0;
                    ReturnFlag flag = cvode.Step(t, y, out t, Task.Normal);
                    if (flag != ReturnFlag.Success)
                    {
                        Console.WriteLine("Error in \"Step\": {0}.", flag);
                        return;
                    }
                    Console.WriteLine($"{t,6:0.000}: {y[0],10:0.000000}   {Math.Sqrt(1.0+t)*Math.Cos(t*t),10:0.000000}");
                    Console.WriteLine($"        {y[1],10:0.000000}   {Math.Sqrt(1.0+t)*Math.Sin(t*t),10:0.000000}");
                }
            }

            //
            // Birta 1993 Problem 4
            //

            Console.WriteLine();
            Console.WriteLine("Birta 1993 Problem 4");
            double eps = 0.9;
            using (var y = new NVectorSerial(new double[] {1.0-eps, 0.0, 0.0, Math.Sqrt((1.0+eps)/(1.0-eps))}))
            using (var A = new SUNDenseMatrix(4, 4))
            using (var solver = new SUNLinSolDense(y, A))
            using (var cvode = new Solver())
            {
                var t = 0.0;
                cvode.Init(new CVRhsFn(Birta1993P4), t, y);
                cvode.SetTolerances(1e-6, 1e-6);
                cvode.SetLinearSolver(solver, A);
                Func<double,double> mu = tt => nr(eps, tt, tt);
                Console.WriteLine($"{t,6:0.000}: {y[0],10:0.000000}   {y[0],10:0.000000}");
                Console.WriteLine($"        {y[1],10:0.000000}   {y[1],10:0.000000}");
                Console.WriteLine($"        {y[2],10:0.000000}   {y[2],10:0.000000}");
                Console.WriteLine($"        {y[3],10:0.000000}   {y[3],10:0.000000}");
                for (int i=1; i<=200; i++)
                {
                    t = (double)i*20.0/200.0;
                    ReturnFlag flag = cvode.Step(t, y, out t, Task.Normal);
                    if (flag != ReturnFlag.Success)
                    {
                        Console.WriteLine("Error in \"Step\": {0}.", flag);
                        return;
                    }
                    double mt = mu(t);
                    Console.WriteLine($"{t,6:0.000}: {y[0],10:0.000000}   {Math.Cos(mt)-eps,10:0.000000}");
                    Console.WriteLine($"        {y[1],10:0.000000}   {Math.Sqrt(1.0-eps*eps)*Math.Sin(mt),10:0.000000}");
                    Console.WriteLine($"        {y[2],10:0.000000}   {-Math.Sin(mt)/(1.0-eps*Math.Cos(mt)),10:0.000000}");
                    Console.WriteLine($"        {y[3],10:0.000000}   {Math.Sqrt(1.0-eps*eps)*Math.Cos(mt)/(1.0-eps*Math.Cos(mt)),10:0.000000}");
                }
            }
        }
    }
}

