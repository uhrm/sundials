
//
//  nvector.cs
//  sundials/cvodes
//
//  Copyright 2011-2019 Markus Uhr <uhrmar@gmail.com>.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in
//        the documentation and/or other materials provided with the
//        distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace nvector
{
    internal enum N_Vector_ID
    {
        SUNDIALS_NVEC_SERIAL,
        SUNDIALS_NVEC_PARALLEL,
        SUNDIALS_NVEC_OPENMP,
        SUNDIALS_NVEC_PTHREADS,
        SUNDIALS_NVEC_PARHYP,
        SUNDIALS_NVEC_PETSC,
        SUNDIALS_NVEC_CUDA,
        SUNDIALS_NVEC_RAJA,
        SUNDIALS_NVEC_OPENMPDEV,
        SUNDIALS_NVEC_TRILINOS,
        SUNDIALS_NVEC_CUSTOM
    }

    public abstract class NVector : IDisposable
    {
        internal readonly IntPtr raw;
        private  readonly bool owner;
        
        internal NVector(IntPtr p, bool o)
        {
            raw = p;
            owner = o;
        }
        
        ~NVector()
        {
            Dispose(false);
        }
        
        // IDisposable
        
        private bool disposed = false;
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (owner)
                    N_VDestroy(raw);
                disposed = true;
            }
        }

        // methods

        public NVector Clone()
        {
            return Wrap(N_VClone(this.raw), true);
        }
        
        public NVectorArray CloneVectorArray(int n)
        {
            return new NVectorArray(N_VCloneVectorArray(n, this.raw), true, n);
        }

        public Span<double> AsSpan()
        {
            unsafe {
                return new Span<double>(N_VGetArrayPointer(this.raw).ToPointer(), Length);
            }
        }

        // properties

        public abstract int Length
        {
            get;
        }

        // indexer

        public double this[int i]
        {
            get {
                return AsSpan()[i];

            }
            set {
                AsSpan()[i] = value;
            }
        }
        
        // static methods

        internal static NVector Wrap(IntPtr p) {
            return Wrap(p, false);
        }

        internal static NVector Wrap(IntPtr p, bool o) {
            var type = N_VGetVectorID(p);
            switch (type) {
                case N_Vector_ID.SUNDIALS_NVEC_SERIAL:
                    return new NVectorSerial(p, o);
                default:
                    throw new ArgumentException($"Unsupported vector type: {type}");
            }
        }
        
        public static void Const(double c, NVector z)
        {
            N_VConst(c, z.raw);
        }
        
        public static void LinearSum(double a, NVector x, double b, NVector y, NVector z)
        {
            N_VLinearSum(a, x.raw, b, y.raw, z.raw);
        }
        
        public static void Prod(NVector x, NVector y, NVector z)
        {
            N_VProd(x.raw, y.raw, z.raw);
        }
        
        public static void Div(NVector x, NVector y, NVector z)
        {
            N_VDiv(x.raw, y.raw, z.raw);
        }
        
        public static void Scale(double c, NVector x, NVector z)
        {
            N_VScale(c, x.raw, z.raw);
        }
        
        public static void Abs(NVector x, NVector z)
        {
            N_VAbs(x.raw, z.raw);
        }
        
        public static void Inv(NVector x, NVector z)
        {
            N_VInv(x.raw, z.raw);
        }
        
        public static void AddConst(NVector x, double b, NVector z)
        {
            N_VAddConst(x.raw, b, z.raw);
        }
        
        public static double DotProd(NVector x, NVector y)
        {
            return N_VDotProd(x.raw, y.raw);
        }
        
        // native methods

        [DllImport("sundials_cvodes")]
        private static extern N_Vector_ID N_VGetVectorID(IntPtr p);
        
        [DllImport("sundials_cvodes")]
        private static extern void N_VDestroy(IntPtr p);
        
        [DllImport("sundials_cvodes")]
        private static extern IntPtr N_VClone(IntPtr p);

        [DllImport("sundials_cvodes")]
        private static extern IntPtr N_VGetArrayPointer(IntPtr v);
        
        [DllImport("sundials_cvodes")]
        private static extern void N_VConst(double c, IntPtr z);
        
        [DllImport("sundials_cvodes")]
        private static extern void N_VLinearSum(double a, IntPtr x, double b, IntPtr y, IntPtr z);
        
        [DllImport("sundials_cvodes")]
        private static extern void N_VProd(IntPtr x, IntPtr y, IntPtr z);
        
        [DllImport("sundials_cvodes")]
        private static extern void N_VDiv(IntPtr x, IntPtr y, IntPtr z);
        
        [DllImport("sundials_cvodes")]
        private static extern void N_VScale(double c, IntPtr x, IntPtr z);
        
        [DllImport("sundials_cvodes")]
        private static extern void N_VAbs(IntPtr x, IntPtr z);
        
        [DllImport("sundials_cvodes")]
        private static extern void N_VInv(IntPtr x, IntPtr z);
        
        [DllImport("sundials_cvodes")]
        private static extern void N_VAddConst(IntPtr x, double b, IntPtr z);
        
        [DllImport("sundials_cvodes")]
        private static extern double N_VDotProd(IntPtr x, IntPtr z);

        [DllImport("sundials_cvodes")]
        private static extern IntPtr N_VCloneVectorArray(int count, IntPtr p);
    }

    // represents an array of NVectors (as returned by CloneVectorArray)
    public class NVectorArray : IDisposable
    {
        internal readonly IntPtr raw;
        private readonly bool owner;
        private readonly int length;
        
        internal NVectorArray(IntPtr r, bool o, int l)
        {
            raw = r;
            owner = o;
            length = l;
        }
        
        ~NVectorArray()
        {
            Dispose(false);
        }
        
        // IDisposable
        
        private bool disposed = false;
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        private void Dispose(bool disposing)
        {
            if (!disposed) {
                if (owner) {
                    N_VDestroyVectorArray(raw, length);
                }
                disposed = true;
            }
        }

        // properties

        public int Length
        {
            get { return length; }
        }

        // indexer

        public NVector this[int i]
        {
            get {
                if (i < 0 || i >= Length) {
                    throw new ArgumentOutOfRangeException(nameof(i), $"Found {nameof(i)} = {i}, but expected 0 <= {nameof(i)} < {Length}");
                }
                IntPtr p = Marshal.ReadIntPtr(this.raw, i*Marshal.SizeOf(typeof(IntPtr)));
                return NVector.Wrap(p);
            }
            set {
                throw new NotImplementedException();
            }
        }
        
        // native methods
        
        [DllImport("sundials_nvecserial")]
        private static extern void N_VDestroyVectorArray(IntPtr vs, int count);
    }

    public class NVectorSerial : NVector
    {
        internal NVectorSerial(IntPtr p, bool o) : base(p, o) { }
        
        public NVectorSerial(int n) : base(N_VNew_Serial(n), true) { }
        
        public NVectorSerial(double[] data) : this(data.Length)
        {
            data.CopyTo(AsSpan());
        }
        
        // properties
        
        public override int Length
        {
            get { return (int)N_VGetLength_Serial(this.raw); }
        }
        
        // native methods
        
        [DllImport("sundials_nvecserial")]
        private static extern IntPtr N_VNew_Serial(int n);

        [DllImport("sundials_nvecserial")]
        private static extern long N_VGetLength_Serial(IntPtr p);
        
        // [DllImport("sundials_nvecserial")]
        // private static extern IntPtr N_VMake_Serial(int n, IntPtr data);
        
        // [DllImport("sundials_nvecserial")]
        // private static extern IntPtr N_VCloneVectorArray_Serial(int count, IntPtr w);
    }
}

