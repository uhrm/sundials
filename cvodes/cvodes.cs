
//
//  cvodes.cs
//  sundials/cvodes
//
//  Copyright 2011-2019 Markus Uhr <uhrmar@gmail.com>.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in
//        the documentation and/or other materials provided with the
//        distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

using System;
using System.Runtime.InteropServices;

using linearsolver;
using sunmatrix;
using nvector;

//using CVSensRhsFn = System.Func<int, double, nvector.NVector, nvector.NVector, nvector.NVectorArray, nvector.NVectorArray, System.IntPtr, nvector.NVector, nvector.NVector, int>;

namespace cvodes
{
    public enum Lmm
    {
        Adams = 1,
        Bdf   = 2
    }

    public enum Iter
    {
        Functional = 1,
        Newton     = 2
    }

    public enum Task
    {
        Normal  = 1,
        OneStep = 2
    }
    
    public enum Ism
    {
        Simultaneous = 1,
        Staggered    = 2,
        Staggered1   = 3
    }
    
    public enum ReturnFlag
    {
        Success                     =   0,
        TStopReturn                 =   1,
        RootReturn                  =   2,
        
        Warning                     =  99,
        
        TooMuchWork                 =  -1,
        TooMuchAcc                  =  -2,
        TooManyErrors               =  -3,
        ConvergenceFailure          =  -4,
        
        LinearSolverInitFail        =  -5,
        LinearSolverSetupFail       =  -6,
        LinearSolverSolveFail       =  -7,
        RhsFuncFail                 =  -8,
        FirstRhsFuncError           =  -9,
        RepeatedRhsFuncError        = -10,
        UnrecoverableRhsFuncError   = -11,
        RootFuncFail                = -12,
        
        MemFail                     = -20,
        MemNull                     = -21,
        IllegalInput                = -22,
        NoMalloc                    = -23,
        BadK                        = -24,
        BadT                        = -25,
        BadDKY                      = -26,
        TooClose                    = -27,
        
        NoQuad                      = -30,
        QRhsFuncError               = -31,
        FirstQRhsFuncError          = -32,
        RepeatedQRhsFuncError       = -33,
        UnrecoverableQRhsFuncError  = -34,
        
        NoSens                      = -40,
        SRhsFuncFail                = -41,
        FirstSRhsFuncError          = -42,
        RepeatedSRhsFuncError       = -43,
        UnrecoverableSRhsFuncError  = -44,
        
        BadIS                       = -45,
        
        NoQuadSens                  = -50,
        QSRhsFuncFail               = -51,
        FirstQSRhsFuncError         = -52,
        RepeatedQSRhsFuncError      = -53,
        UnrecoverableQSRhsFuncError = -54
    }
    
    public delegate int CVRhsFn(double t, NVector y, NVector ydot, IntPtr user_data);
    internal delegate int InternalCVRhsFn(double t, IntPtr y, IntPtr ydot, IntPtr user_data);
    
    public delegate int CVSensRhsFn(int ns, double t, NVector y, NVector ydot, NVectorArray yS, NVectorArray ySdot, IntPtr user_data, NVector tmp1, NVector tmp2);
    internal delegate int InternalCVSensRhsFn(int ns, double t, IntPtr y, IntPtr ydot, IntPtr yS, IntPtr ySdot, IntPtr user_data, IntPtr tmp1, IntPtr tmp2);

    public class Solver : IDisposable
    {
        internal class CVRhsFnWrapper
        {
            private CVRhsFn func;
            public CVRhsFnWrapper(CVRhsFn f)
            {
                if (f == null)
                    throw new ArgumentNullException(nameof(f));
                func = f;
            }
            public int Call(double t, IntPtr y, IntPtr ydot, IntPtr user_data)
            {
                return func(t, new NVectorSerial(y, false), new NVectorSerial(ydot, false), user_data);
            }
        }
        
        internal class CVSensRhsFnWrapper
        {
            private CVSensRhsFn func;
            public CVSensRhsFnWrapper(CVSensRhsFn f)
            {
                if (f == null)
                    throw new ArgumentNullException(nameof(f));
                func = f;
            }
            public int Call(int ns, double t, IntPtr y, IntPtr ydot, IntPtr yS, IntPtr ySdot, IntPtr user_data, IntPtr tmp1, IntPtr tmp2)
            {
                return func(ns, t, NVector.Wrap(y), NVector.Wrap(ydot), new NVectorArray(yS, false, ns), new NVectorArray(ySdot, false, ns), user_data, NVector.Wrap(tmp1), NVector.Wrap(tmp2));
            }
        }

        IntPtr raw;
        InternalCVRhsFn fwrapper;
        InternalCVSensRhsFn fSwrapper;

        public Solver() : this(Lmm.Bdf, Iter.Newton) {}
        
        public Solver(Lmm lmm, Iter iter)
        {
            raw = CVodeCreate((int)lmm, (int)iter);
        }
        
        ~Solver()
        {
            Dispose(false);
        }
        
        // IDisposable
        
        private bool disposed = false;
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                CVodeFree(ref raw);
                raw = IntPtr.Zero;
                fwrapper = null;
                fSwrapper = null;
                disposed = true;
            }
        }
        
        // CVODES methods
        
        public void Init(CVRhsFn f, double t0, double[] y0)
        {
            using (NVector yw = new NVectorSerial(y0))
                Init(f, t0, yw);
        }
        
        public void Init(CVRhsFn f, double t0, NVector y0)
        {
            fwrapper = new InternalCVRhsFn((new CVRhsFnWrapper(f)).Call);
            int flag = CVodeInit(raw, Marshal.GetFunctionPointerForDelegate(fwrapper), t0, y0.raw);
            if (flag != 0)
                throw new ApplicationException($"Error initializing CVODE: {flag}.");
        }
        
        public void SetTolerances(double rtol, double atol)
        {
            int flag = CVodeSStolerances(raw, rtol, atol);
            if (flag != 0)
                throw new ApplicationException($"Error setting tolerances: {flag}.");
        }
        
        public void SetTolerances(double rtol, double[] atol)
        {
            int flag;
            using (NVector atw = new NVectorSerial(atol))
                flag = CVodeSVtolerances(raw, rtol, atw.raw);
            if (flag != 0)
                throw new ApplicationException($"Error setting tolerances: {flag}.");
        }
        
        public void SetMinStep(double hmin)
        {
            int flag = CVodeSetMinStep(raw, hmin);
            if (flag != 0)
                throw new ApplicationException($"Error setting min. step: {flag}.");
        }
        
        public void SetMaxStep(double hmax)
        {
            int flag = CVodeSetMaxStep(raw, hmax);
            if (flag != 0)
                throw new ApplicationException($"Error setting max. step: {flag}.");
        }
        
        public void SetStopTime(double tstop)
        {
            int flag = CVodeSetStopTime(raw, tstop);
            if (flag != 0)
                throw new ApplicationException($"Error setting stop time: {flag}.");
        }
        
        public void SetIterType(Iter iter)
        {
            int flag = CVodeSetIterType(raw, (int)iter);
            if (flag != 0)
                throw new ApplicationException($"Error setting iteration type: {flag}.");
        }
        
        public void SetLinearSolver(SUNLinearSolver solver, SUNMatrix A)
        {
            var flag = CVodeSetLinearSolver(raw, solver.Raw, A.Raw);
            if (flag != 0) {
                throw new ApplicationException($"Error setting dense solver: {flag}.");
            }
        }

        public ReturnFlag Step(double tout, double[] yout, out double tret, Task task)
        {
            int n = yout.Length;
            ReturnFlag flag;
            using (NVector yw = new NVectorSerial(n))
            {
                flag = Step(tout, yw, out tret, task);
                yw.AsSpan().CopyTo(yout);
            }
            return flag;
        }
        
        // F# compatible ordering of "out" parameter
        public ReturnFlag Step(double tout, NVector yout, Task task, out double tret)
        {
            return Step(tout, yout, out tret, task);
        }
        
        public ReturnFlag Step(double tout, NVector yout, out double tret, Task task)
        {
            int flag = CVode(raw, tout, yout.raw, out tret, (int)task);
            if (flag < 0)
                throw new ApplicationException($"Error in Step: {(ReturnFlag)flag}.");
            return (ReturnFlag)flag;
        }
        
        // sensitivity methods
        
        public void SensInit(int ns, Ism ism, CVSensRhsFn fS, NVectorArray yS0)
        {
            fSwrapper = new InternalCVSensRhsFn((new CVSensRhsFnWrapper(fS)).Call);
            int flag = CVodeSensInit(raw, ns, (int)ism, Marshal.GetFunctionPointerForDelegate(fSwrapper), yS0.raw);
            if (flag < 0)
                throw new ApplicationException($"Error initializing sensitivities: {(ReturnFlag)flag}.");
        }
        
        public void SensSetSSTolerances(double rtol, double[] atol)
        {
            int flag = CVodeSensSStolerances(raw, rtol, atol);
            if (flag < 0)
                throw new ApplicationException($"Error setting sensitivity tolerances: {(ReturnFlag)flag}.");
        }
        
        public void SensSetEETolerances()
        {
            int flag = CVodeSensEEtolerances(raw);
            if (flag < 0)
                throw new ApplicationException($"Error setting sensitivity tolerances: {(ReturnFlag)flag}.");
        }
        
        public void GetSens(out double tret, NVectorArray yS)
        {
            int flag = CVodeGetSens(raw, out tret, yS.raw);
            if (flag < 0)
                throw new ApplicationException($"Error getting sensitivities: {(ReturnFlag)flag}.");
        }

        // F# compatible ordering of "out" parameter
        public void GetSens(NVectorArray yS, out double tret)
        {
            int flag = CVodeGetSens(raw, out tret, yS.raw);
            if (flag < 0)
                throw new ApplicationException($"Error getting sensitivities: {(ReturnFlag)flag}.");
        }
        
        // native methods
        
        [DllImport("sundials_cvodes")]
        private static extern IntPtr CVodeCreate(int lmm, int iter);
        
        [DllImport("sundials_cvodes")]
        private static extern void CVodeFree(ref IntPtr p);
        
        [DllImport("sundials_cvodes")]
        private static extern int CVodeInit(IntPtr p, IntPtr f, double t0, IntPtr y0);
        
        [DllImport("sundials_cvodes")]
        private static extern int CVodeSStolerances(IntPtr p, double reltol, double abstol);
        
        [DllImport("sundials_cvodes")]
        private static extern int CVodeSVtolerances(IntPtr p, double reltol, IntPtr abstol);
        
        [DllImport("sundials_cvodes")]
        private static extern int CVodeSetMinStep(IntPtr p, double hmin);
        
        [DllImport("sundials_cvodes")]
        private static extern int CVodeSetMaxStep(IntPtr p, double hmax);
        
        [DllImport("sundials_cvodes")]
        private static extern int CVodeSetStopTime(IntPtr p, double tstop);
        
        [DllImport("sundials_cvodes")]
        private static extern int CVodeSetIterType(IntPtr p, int iter);
        
        [DllImport("sundials_cvodes")]
        private static extern int CVodeSetLinearSolver(IntPtr p, IntPtr solver, IntPtr A);
        
        [DllImport("sundials_cvodes")]
        private static extern int CVode(IntPtr p, double tout, IntPtr yout, out double tret, int task);

        [DllImport("sundials_cvodes")]
        private static extern int CVodeSensInit(IntPtr p, int ns, int ism, IntPtr fS, IntPtr yS0);

        [DllImport("sundials_cvodes")]
        private static extern int CVodeSensSStolerances(IntPtr p, double reltolS, double[] abstolS);

        [DllImport("sundials_cvodes")]
        private static extern int CVodeSensEEtolerances(IntPtr p);

        [DllImport("sundials_cvodes")]
        private static extern int CVodeGetSens(IntPtr p, out double tret, IntPtr yS);
    }
}

