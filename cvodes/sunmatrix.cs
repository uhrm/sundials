
//
//  sunmatrix.cs
//  sundials/cvodes
//
//  Copyright 2011-2019 Markus Uhr <uhrmar@gmail.com>.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in
//        the documentation and/or other materials provided with the
//        distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace sunmatrix
{
    public abstract class SUNMatrix : IDisposable
    {
        private readonly IntPtr raw;

        private readonly Func<IntPtr,int> free;

        internal SUNMatrix(IntPtr p, Func<IntPtr,int> f)
        {
            this.raw = p;
            this.free = f;
        }
        
        ~SUNMatrix()
        {
            Dispose(false);
        }
        
        // IDisposable
        
        private bool disposed = false;
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        private void Dispose(bool disposing)
        {
            if (!this.disposed) {
                this.free(raw);
                this.disposed = true;
            }
        }

        // properties

        internal IntPtr Raw
        {
            get { return raw; }
        }
    }

    public class SUNDenseMatrix : SUNMatrix
    {
        public SUNDenseMatrix(long m, long n)
        : base(_SUNDenseMatrix(m, n), SUNMatDestroy) { }

        // native methods

        [DllImport("sundials_sunmatrixdense", EntryPoint="SUNDenseMatrix")]
        private static extern IntPtr _SUNDenseMatrix(long m, long n);

        [DllImport("sundials_sunmatrixdense")]
        private static extern int SUNMatDestroy(IntPtr p);
    }
}

